function configureMyTheme(site) {
    browser.theme.update({
        colors: {
            accentcolor: site.color,
            textcolor: '#000' //Need this one or else theme won't work
        }
    });
}

function configureEmptyTheme() {
    browser.theme.reset();
}

async function configureContentOverlay(site, tabId) {
    //Also run the content script to change in-page styling
    
    await browser.tabs.executeScript(tabId, {
        code: "distinct__siteJson = " + JSON.stringify(site) + "; "
    });
    
    await browser.tabs.executeScript(tabId, {
        file: '/distinct.js',
        //allFrames: true 
    });
}

function siteMatches(siteDefinition, url) {
    if (!url)
        return false;
    
    if (siteDefinition.pattern.startsWith('/') && siteDefinition.pattern.endsWith('/')) {
        const reg = new RegExp(siteDefinition.pattern.substring(1, siteDefinition.pattern.length - 1), 'i');
        return url.match(reg);
    } else {
        return url.toLowerCase().indexOf(siteDefinition.pattern.toLowerCase()) >= 0;
    }
}

async function activeTabUpdateHandler(tabId) {
    await browser.tabs.get(tabId, async (tab) => {
        //Only interested in active tabs
        if (!tab.active) {
            return;
        }
        
        const config = (await browser.storage.sync.get(["sites", "overlayAmount"])) || {};
        const sites = config.sites || [];
        let overlayAmount = config.overlayAmount;
        if (overlayAmount === undefined)
            overlayAmount = 0.25; //TODO refactor this defaulting code, it's in 2 places
        
        const matchingSite = sites.find(site => siteMatches(site, tab.url));
        if (matchingSite) {
            configureMyTheme(matchingSite);
            if (overlayAmount > 0) {
                await configureContentOverlay({...matchingSite, overlayAmount}, tabId);
            }
        } else {
            configureEmptyTheme();
        }
    });
}

browser.tabs.onActivated.addListener(activeInfo => activeTabUpdateHandler(activeInfo.tabId));
browser.tabs.onUpdated.addListener(tabId => activeTabUpdateHandler(tabId));

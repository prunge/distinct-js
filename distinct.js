//distinct__siteJson is being passed through from the distinct extension with color and style information
if (distinct__siteJson) {
    let site = distinct__siteJson;

    //Create an overlay for the whole site that makes it a tinge of a known distinct color
    overlayDiv = document.createElement('div');
    overlayDiv.id = 'distint-extension-overlay';
    overlayDiv.style.position = 'fixed';
    overlayDiv.style.width = '100%';
    overlayDiv.style.height = '100%';
    overlayDiv.style.top = 0;
    overlayDiv.style.left = 0;
    overlayDiv.style.bottom = 0;
    overlayDiv.style.right = 0;
    overlayDiv.style.zIndex = 1000000; //Enough to be over the top of everything
    overlayDiv.style.pointerEvents = 'none'; //Allow cclick through
    const overlayAlphaHex = Math.round(site.overlayAmount * 255).toString(16);
    overlayDiv.style.backgroundColor = site.color + overlayAlphaHex;
    existingOverlayDiv = document.getElementById(overlayDiv.id);
    if (existingOverlayDiv) {
        document.body.removeChild(existingOverlayDiv);
    }
    document.body.appendChild(overlayDiv);
}

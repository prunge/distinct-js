# Distinct

A Firefox extension that applies themes to site whose URLs match patterns to give them a distinct look in the browser.

Can be used when working with testing and production environments to make it obvious which environment is being used.
const autoSaveEnabled = true; //Set to false to require save button to save

function prepareSaveOptions() {
    const sites = [];
    let siteIndex = 0;
    let curSitePatternElement, curSiteColorElement;
    do {
        curSitePatternElement = document.querySelector("#site-" + siteIndex);
        curSiteColorElement = document.querySelector("#color-" + siteIndex);

        if (curSitePatternElement && curSiteColorElement) {
            const curSiteEntry = {
                pattern: curSitePatternElement.value,
                color: curSiteColorElement.value
            };
            if (curSiteEntry.pattern.trim().length > 0)
                sites.push(curSiteEntry);
        }
        siteIndex++;
    } while (curSitePatternElement && curSiteColorElement);

    const overlayAmount = document.querySelector("#overlayAmount").value / 100.0;
    
    return {sites: sites, overlayAmount: overlayAmount};
}

function saveOptions(e) {
    e.preventDefault();
    browser.storage.sync.set(prepareSaveOptions());
}

function restoreOptions() {

    function setConfig(config) {
        setSites(config);
        setOverlayAmount(config);
    }
    
    function setSites(config) {
        
        const sites = config.sites || [];
        let siteIndex = 0;
        for (const site of sites) {
            
            //Fill in as many rows as we need to show config for this item
            let numRowsCreated = 0; //Use limit of 1000 just in case - otherwise we crash whole browser
            while (!document.querySelector("#site-" + siteIndex) && numRowsCreated < 1000) {
                addTableRow();
                numRowsCreated++;
            }
            
            document.querySelector("#site-" + siteIndex).value = site.pattern;
            document.querySelector("#color-" + siteIndex).value = site.color;
            siteIndex++;
        }

        //An extra row so the user can add a new site straight away
        addTableRow();
    }
    
    function setOverlayAmount(config) {
        let overlayAmount = config.overlayAmount;
        if (overlayAmount === undefined) {
            overlayAmount = 0.25; //TODO default overlay logic duplicated
        }

        document.querySelector("#overlayAmount").value = overlayAmount * 100;
        updateOverlayAmount();
    }

    function onError(error) {
        console.log(`Error: ${error}`);
    }
    
    const getting = browser.storage.sync.get(["sites", "overlayAmount"]);
    getting.then(setConfig, onError);
}

function removeTableRow(rowIndex) {
    //Just clean out and hide the row for now, on reload blank pattern rows are removed anyway
    document.querySelector("#site-" + rowIndex).value = '';
    document.querySelector("#row-" + rowIndex).style.display = 'none';
    autosave();
}

function addTableRow() {
    const table = document.querySelector("#siteTable");
    const rowIndex = table.rows.length - 1;  //less one for header
    const row = table.insertRow();
    row.id = "row-" + rowIndex;
    const colorCell = row.insertCell();
    const patternCell = row.insertCell();
    const removeCell = row.insertCell();

    const removeButton = document.createElement("button");
    removeButton.appendChild(document.createTextNode('-'));
    //<button id="remove-0" title="Remove this row">
    removeButton.id = "remove-" + rowIndex;
    removeButton.title = "Remove this row";
    removeButton.addEventListener("click", ev => { removeTableRow(rowIndex); ev.preventDefault(); });
    removeCell.appendChild(removeButton);
    
    const colorInput = document.createElement("input");
    //<input id="color-0" type="color" title="Color">
    colorInput.id = "color-" + rowIndex;
    colorInput.type = "color";
    colorInput.title = "Color to use for the browser theme and page overlay";
    colorInput.value = '#FFAAAA';
    colorCell.appendChild(colorInput);
    
    const patternInput = document.createElement("input");
    //<input id="site-0" title="Site match pattern" style="width: 100%">
    patternInput.id = "site-" + rowIndex;
    patternInput.title = "Site match pattern.  Uses simple string matching by default on the site URL.  Enclose pattern between '/' characters to use a regular expression instead.";
    patternInput.style.width = '100%';
    patternCell.appendChild(patternInput);

    if (autoSaveEnabled) {
        colorInput.addEventListener("change", ev => autosave());
        patternInput.addEventListener("change", ev => autosave());
    }
}

async function autosave() {
    const dataToSave = prepareSaveOptions();
    console.log("Would autosave here!", dataToSave);
    
    if (autoSaveEnabled) {
        await browser.storage.sync.set(dataToSave);
    }
}

function updateOverlayAmount() {
    const overlayValue = document.querySelector("#overlayAmount").value;
    let description = overlayValue + ' %';
    if (overlayValue == 0) {
        description = 'disabled';
    }
    document.querySelector("#overlayAmountInfo").innerText = description;
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.addEventListener("DOMContentLoaded", updateOverlayAmount);
document.querySelector("form").addEventListener("submit", saveOptions);
document.querySelector("#newRowButton").addEventListener("click", ev => { addTableRow(); ev.preventDefault(); });
document.querySelector("#overlayAmount").addEventListener("input", ev => updateOverlayAmount());

if (autoSaveEnabled) {
    document.querySelector("#overlayAmount").addEventListener("change", ev => autosave());
}

if (autoSaveEnabled) {
    document.querySelector("#saveButton").style.display = "none";
}


//Store settings in the following format:
/*
sites: [
    {
        pattern: "mozilla",
        color: "#ff0000"
    },
],
overlayAmount: 0.2
 */
